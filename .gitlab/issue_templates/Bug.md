<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "bug" label:

- https://gitlab.com/slumber/multi-user/-/issues?scope=all&utf8=✓&label_name[]=bug

and verify the issue you're about to submit isn't a duplicate.
--->

### Summary

(Summarize the bug encountered concisely)

* Addon version: (your addon-version)
* Blender version: (your blender version)
* OS: (your os windows/linux/mac)


### Steps to reproduce

(How one can reproduce the issue - this is very important)

###  Example Project [optionnal]
(If possible, please create an example project  that exhibits the problematic behavior, and link to it here in the bug report)


### What is the current *bug* behavior?

(What actually happens)


### Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's tough to read otherwise.)


### Possible fixes [optionnal]

(If you can, link to the line of code that might be responsible for the problem)


/label ~type::bug
/cc @project-manager
